## VŠE-BUG backend
- Tento repozitář slouží jako reprezentace části školního týmového projektu Všebug
realizovaného Janem Pfeifferem, Janem Podestátem, Lukášem Vávrou. 
Celý projekt se skládá ze dvou částí propojených pomocí REST API a dělí se na 
Frontend a Backend. Frontend je desktopová Java FX aplikace a pracovali na ní
Jan Podestát a Lukáš Vávra. Backend je realizovaný pomocí Django REST framework a pracoval
na něm Jan Pfeiffer. Aplikace slouží k administraci IT projektu. V aplikaci lze nahlásit
issues a přiřadit je uživatelům.
- doména: https://vsebug-be.herokuapp.com
- engine: django rest frmework s `Django==2.1.7`
- hosting: heroku.com
- db: postgresql

### Seznam urls
- https://vsebug-be.herokuapp.com/login/ - POST (nevyžaduje autorizaci)
```json
{
  "username": "string",
  "password": "string"
}
```
- https://vsebug-be.herokuapp.com/register/ - POST (nevyžaduje autorizaci)
```json
{
  "username": "string",
  "password": "string",
  "role": null
}
```
- https://vsebug-be.herokuapp.com/roles/ - POST, GET (nevyžaduje autorizaci)
```json
{
  "name": "string"
}
```
- https://vsebug-be.herokuapp.com/roles/id/ - GET, PATCH, PUT, DELETE (nevyžaduje autorizaci)
- https://vsebug-be.herokuapp.com/states/ - POST, GET (vyžaduje autorizaci)
```json
{
  "name": "string"
}
```
- https://vsebug-be.herokuapp.com/states/id/ - GET, PATCH, PUT, DELETE (vyžaduje autorizaci)
- https://vsebug-be.herokuapp.com/projects/ - POST, GET (vyžaduje autorizaci)
```json
{
  "name": "string",
  "created": "string"
}
```
- https://vsebug-be.herokuapp.com/projects/id/ - GET, PATCH, PUT, DELETE (vyžaduje autorizaci)
- https://vsebug-be.herokuapp.com/issues/ - POST, GET (vyžaduje autorizaci)
```json
{
  "name": "string",
  "description": "string",
  "created": "string",
  "project": 1,
  "state": null,
  "assignee": null
}
```
- https://vsebug-be.herokuapp.com/issues/id/ - GET, PATCH, PUT, DELETE (vyžaduje autorizaci)
- https://vsebug-be.herokuapp.com/users/ - GET (vyžaduje autorizaci)

### Autorizace
1) Uživatel se přihlásí přes POST https://vsebug-be.herokuapp.com/login/
```json
{
    "username": "username",
    "password": "password"
}
```
2) V response dostane access token
```json
{
    "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTU5MDQxOTM0NiwianRpIjoiNDkyZjllMDkzZjUxNDcwZDkzYjcxMzY2Mjk1ODRiNzgiLCJ1c2VyX2lkIjoxfQ._4GpUePja3McT-XAeBQnRs0506lyZ382GYoxGR8-gBk",
    "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTkwMzMzMjQ2LCJqdGkiOiI5NGU4NmY2MjRiZWI0YTVkOTllZTI1ZDJjOGVjNDk3ZSIsInVzZXJfaWQiOjF9.bG2s_nH5wF1DRkZZv5cSZvmdbBfpqfEk7xU1hqkWmAI"
}
```
3) Pokud uživatel chce získat data v requestu který vyžaduje authorizaci, tak vytvoří hlavičku authorization a k ní jako hodnotu `Bearer + access token`
```
GET https://vsebug-be.herokuapp.com/projects/ HTTP/1.1
Accept-Encoding: gzip,deflate
authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTkwMzMzNTYwLCJqdGkiOiI3MWExZDY3OGMxODc0ZmVjOGJiMzY1NmM1M2NlNzg4NSIsInVzZXJfaWQiOjN9.sVl9fY-tO3n6Oe4M-s6cENXYfJIPHbEtVRO0G97B9_g
Host: vsebug-be.herokuapp.com
Connection: Keep-Alive
User-Agent: Apache-HttpClient/4.1.1 (java 1.5)
```
### Vyhledávání
vyhledávání je aplikované na projekty a issues. Je možné vyhledat danou instanci podle jejího atributu `name`
- GET https://vsebug-be.herokuapp.com/projects/?search=
- GET https://vsebug-be.herokuapp.com/issues/?search=
Uživatel zadá hledaný název za `?search=` a je možné použít mezery velká písmena a diakritiku, například:
- GET https://vsebug-be.herokuapp.com/issues/?search=Issue 232


