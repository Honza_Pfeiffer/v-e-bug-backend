from rest_framework import serializers
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework import exceptions
from . import models

class RoleSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = models.State
        fields = ('id', 'name')

class UserSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    role = serializers.SlugRelatedField(read_only=True, slug_field='name')
    class Meta:
        model = models.ApiUser
        fields = ('id', 'username', 'email', 'role')

class StateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = models.State
        fields = ('id', 'name')

class BaseIssueSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    assignee = UserSerializer(read_only=True)
    state = serializers.SlugRelatedField(read_only=True, slug_field='name')

    class Meta:
        model = models.Issue
        fields = ('id', 'name', 'description', 'created', 'project', 'state', 'assignee')

class IssueSerializer(BaseIssueSerializer):
    assignee = serializers.PrimaryKeyRelatedField(queryset=models.ApiUser.objects.all(), write_only=True, allow_null=True)
    state = serializers.PrimaryKeyRelatedField(queryset=models.State.objects.all(), write_only=True, allow_null=True)

    def to_representation(self, instance):
        return BaseIssueSerializer(
            instance, context=self.context
        ).data

class ProjectSerializer(serializers.ModelSerializer):
    issues = BaseIssueSerializer(many=True, read_only=True)
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = models.Project
        fields = ('id', 'name', 'created', 'issues')

class StateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    class Meta:
        model = models.State
        fields = ('id', 'name')

class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    #role = serializers.IntegerField(write_only=True, allow_null=True)

    class Meta:
        model = models.ApiUser
        fields = ("id","username", "password", "role")

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user



"""
class BaseIssueSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    assignee = UserSerializer(read_only=True)
    state = StateSerializer(read_only=True)
    #project = serializers.HyperlinkedIdentityField(view_name='project')

    class Meta:
        model = models.Issue
        fields = ('id', 'name', 'description', 'created', 'project', 'state', 'assignee')

class IssueSerializer(BaseIssueSerializer):
    assignee = serializers.PrimaryKeyRelatedField(queryset=models.ApiUser.objects.all(), write_only=True, allow_null=True)
    state = serializers.PrimaryKeyRelatedField(queryset=models.State.objects.all(), write_only=True, allow_null=True)

    def to_representation(self, instance):
        return BaseIssueSerializer(
            instance, context=self.context
        ).data

"""

