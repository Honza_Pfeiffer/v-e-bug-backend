from django.shortcuts import render
from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import permissions, viewsets, generics, filters
from django_filters import rest_framework as filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework_swagger import renderers, views
from rest_framework.views import APIView
from rest_framework.schemas import SchemaGenerator
from rest_framework.response import Response
from . import serializers
from . import models

class SwaggerSchemaView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]


    def get(self, request):
        generator = SchemaGenerator()
        schema = generator.get_schema(request = request)

        return Response(schema)

class ProjectViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.ProjectSerializer
    queryset = models.Project.objects.all()
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('name',)

class IssueViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.IssueSerializer
    queryset = models.Issue.objects.all()
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('name',)

class IssuesByProjectView(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.IssueSerializer


    def get_queryset(self):
        project = get_object_or_404(models.Project, id = self.kwargs["id"])
        qs = models.Issue.objects.filter(project = project)
        return qs


    def get_serializer_context(self):
        return {"request": self.request}


class StateViewList(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.StateSerializer
    queryset = models.State.objects.all()

class RoleViewList(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.RoleSerializer
    queryset = models.Role.objects.all()

class UserViewList(ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.UserSerializer
    queryset = models.ApiUser.objects.all()
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('username',)

class RegisterUserView(CreateAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserRegisterSerializer
    queryset = models.ApiUser.objects.all()

