from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)

#Sekce user managementu-----------------------------------------------------------------------------------

class CustomUserManager(BaseUserManager):
    def create_user(self, username, password, **extra_fields):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        user = self.model(
            username=username,
            **extra_fields
        )
        user.save(using=self._db)
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_staff", True)

        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser set to True.")

        return self.create_user(username, password, **extra_fields)

class Role(models.Model):
    name = models.CharField(max_length=30)

class ApiUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(max_length=30, unique=True, null=False)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
        null=True,
        blank=True
    )
    password = models.CharField(
        max_length=255, null=False, blank=False, verbose_name='password'
    )
    role = models.ForeignKey(Role, on_delete=models.SET_NULL, null=True, blank=True, related_name = 'role')

    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)

    objects = CustomUserManager()

    USERNAME_FIELD = 'username'
    EMAIL_FIELD = 'email'

#Sekce modelů aplikace-----------------------------------------------------------------------------------------------
class Project(models.Model):
    name = models.CharField(max_length=150)
    created = models.DateTimeField(blank=True, null=True)

class State(models.Model):
    name = models.CharField(max_length=30)

class Issue(models.Model):
    name = models.CharField(max_length=150)
    description = models.CharField(max_length=150, null=True, blank=True)
    created = models.DateTimeField(blank=True, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='project')
    state = models.ForeignKey(State, on_delete=models.SET_NULL, blank=True, null=True, related_name = 'state')
    assignee = models.ForeignKey("api.ApiUser", on_delete=models.SET_NULL, blank=True, null=True, related_name = 'assignee')

