"""tymovyProjektBE URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework_simplejwt.views import token_obtain_pair, TokenRefreshView
from rest_framework_simplejwt.views import TokenObtainPairView
from django.contrib import admin
from django.urls import path
from rest_framework import routers
from django.conf.urls import include
from api import views

router = routers.DefaultRouter()

router.register('projects', views.ProjectViewSet)
router.register('issues', views.IssueViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    path('docs/', views.SwaggerSchemaView.as_view()),
    path('projects/', views.ProjectViewSet),
    path('issues/', views.IssueViewSet),
    path('projects/<int:id>/issues/', views.IssuesByProjectView.as_view(), name='project'),
    path('states/', views.StateViewList),
    path('roles/', views.RoleViewList),
    path('register/', views.RegisterUserView.as_view()),
    path("login/", token_obtain_pair),
    path('users/', views.UserViewList.as_view())
]
